<?php
declare(strict_types=1);

namespace Grifix\ProcessManager\Exceptions;

final class ProcessAlreadyExistsException extends \Exception
{
    public function __construct(public readonly string $processId)
    {
        parent::__construct(sprintf('Process [%s] already exists!', $processId));
    }
}
