<?php
declare(strict_types=1);

namespace Grifix\ProcessManager\Exceptions;

final class CannotStartProcessException extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
