<?php
declare(strict_types=1);

namespace Grifix\ProcessManager\Exceptions;

final class TooLongProcessIdException extends \Exception
{

    public function __construct(int $maxLength, string $processId)
    {
        parent::__construct(sprintf('The process id [%s] is too long, maximal length is [%s] characters!', $processId, $maxLength));
    }
}
