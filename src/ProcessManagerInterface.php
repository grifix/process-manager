<?php
declare(strict_types=1);

namespace Grifix\ProcessManager;

use Grifix\ProcessManager\Exceptions\CannotStartProcessException;
use Grifix\ProcessManager\Exceptions\CannotStopProcessException;
use Grifix\ProcessManager\Exceptions\ProcessAlreadyExistsException;
use Grifix\ProcessManager\Exceptions\ProcessDoesNotExistException;

interface ProcessManagerInterface
{
    public function processExists(string $processId): bool;

    /**
     * @throws CannotStartProcessException
     * @throws ProcessAlreadyExistsException
     */
    public function startProcess(string $processId, string $command): void;

    /**
     * @throws CannotStopProcessException
     * @throws ProcessDoesNotExistException
     */
    public function stopProcess(string $processId): void;

    public function findProcesses(?string $filter = null): array;
}
