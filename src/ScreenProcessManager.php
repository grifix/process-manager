<?php

declare(strict_types=1);

namespace Grifix\ProcessManager;

use Grifix\ProcessManager\CommandExecutor\CommandExecutorInterface;
use Grifix\ProcessManager\CommandExecutor\ShellCommandExecutor;
use Grifix\ProcessManager\Exceptions\CannotStartProcessException;
use Grifix\ProcessManager\Exceptions\CannotStopProcessException;
use Grifix\ProcessManager\Exceptions\ProcessAlreadyExistsException;
use Grifix\ProcessManager\Exceptions\ProcessDoesNotExistException;
use Grifix\ProcessManager\Exceptions\TooLongProcessIdException;

final class ScreenProcessManager implements ProcessManagerInterface
{

    private const MAX_PROCESS_ID_LENGTH = 80;

    public function __construct(private readonly CommandExecutorInterface $commandExecutor)
    {
    }


    public function processExists(string $processId): bool
    {
        return boolval($this->commandExecutor->execute(sprintf('screen -list | grep ".%s\b"', $processId))->output);
    }

    /**
     * @throws CannotStartProcessException
     * @throws ProcessAlreadyExistsException
     * @throws TooLongProcessIdException
     */
    public function startProcess(string $processId, string $command): void
    {
        $this->assertProcessIdLength($processId);
        if ($this->processExists($processId)) {
            throw new ProcessAlreadyExistsException($processId);
        }
        $result = $this->commandExecutor->execute(sprintf('screen -dm -S "%s" %s', $processId, $command));
        if ($result->code !== 0) {
            throw new CannotStartProcessException(implode(' ', $result->output));
        }
    }


    /**
     * @throws CannotStopProcessException
     * @throws ProcessDoesNotExistException
     */
    public function stopProcess(string $processId): void
    {
        if (false === $this->processExists($processId)) {
            throw new ProcessDoesNotExistException($processId);
        }
        $result = $this->commandExecutor->execute(sprintf('screen -X -S "%s" quit', $processId));
        if ($result->code !== 0) {
            throw new CannotStopProcessException(implode(' ', $result->output));
        }
    }

    public function findProcesses(?string $filter = null): array
    {
        $result = [];
        $command = 'screen -list';
        if ($filter) {
            $command .= sprintf(' | grep "%s"', $filter);
        }
        $output = $this->commandExecutor->execute($command)->output;
        if (empty($output)) {
            return [];
        }
        if (null === $filter) {
            array_shift($output);
        }
        foreach ($output as $row) {
            if (empty($row)) {
                continue;
            }
            $row = explode('.', $row);
            $row = explode("\t", $row[1]);
            $result[] = $row[0];
        }
        return $result;
    }

    private function assertProcessIdLength($processId): void
    {
        if (strlen($processId) > self::MAX_PROCESS_ID_LENGTH) {
            throw new TooLongProcessIdException(self::MAX_PROCESS_ID_LENGTH, $processId);
        }
    }

    public function create(): self
    {
        return new self(new ShellCommandExecutor());
    }
}
