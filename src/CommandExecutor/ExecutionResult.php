<?php

declare(strict_types=1);

namespace Grifix\ProcessManager\CommandExecutor;

final class ExecutionResult
{
    public function __construct(
        public readonly int $code,
        public readonly array $output,
    ) {
    }
}
