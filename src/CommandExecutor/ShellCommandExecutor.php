<?php

declare(strict_types=1);

namespace Grifix\ProcessManager\CommandExecutor;

final class ShellCommandExecutor implements CommandExecutorInterface
{

    public function execute(string $command): ExecutionResult
    {
        $resultCode = null;
        $output = [];
        exec($command, $output, $resultCode);

        return new ExecutionResult((int)$resultCode, $output);
    }
}
