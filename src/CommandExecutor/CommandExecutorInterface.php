<?php

declare(strict_types=1);

namespace Grifix\ProcessManager\CommandExecutor;

interface CommandExecutorInterface
{
    public function execute(string $command): ExecutionResult;
}
