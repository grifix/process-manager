<?php

declare(strict_types=1);

namespace Grifix\ProcessManager\Tests;

use Grifix\ProcessManager\CommandExecutor\CommandExecutorInterface;
use Grifix\ProcessManager\CommandExecutor\ExecutionResult;
use Grifix\ProcessManager\Exceptions\CannotStartProcessException;
use Grifix\ProcessManager\Exceptions\ProcessAlreadyExistsException;
use Grifix\ProcessManager\Exceptions\ProcessDoesNotExistException;
use Grifix\ProcessManager\ScreenProcessManager;
use PHPUnit\Framework\TestCase;

final class ProcessManagerTest extends TestCase
{
    /**
     * @dataProvider processExistsDataProvider
     */
    public function testItChecksThatProcessExists(
        ExecutionResult $executionResult,
        bool $expectedResult,
        string $processId
    ): void {
        $commandExecutor = \Mockery::mock(CommandExecutorInterface::class)->allows('execute')
            ->andReturn($executionResult)->getMock();
        $processManager = new ScreenProcessManager($commandExecutor);
        self::assertEquals($expectedResult, $processManager->processExists($processId));
    }

    public function processExistsDataProvider(): array
    {
        return [
            'existing process' => [new ExecutionResult(0, ['339397.test	(Detached)']), true, 'test'],
            'not existing process' => [new ExecutionResult(0, []), false, 'test'],
        ];
    }

    public function testItStartsProcess(): void
    {
        $commandExecutor = \Mockery::mock(CommandExecutorInterface::class)
            ->shouldReceive('execute')->once()->andReturn(new ExecutionResult(0, []))->getMock()
            ->shouldReceive('execute')->once()
            ->with('screen -dm -S "test" ls')->andReturn(new ExecutionResult(0, []))->getMock();
        $processManager = new ScreenProcessManager($commandExecutor);
        $processManager->startProcess('test', 'ls');
        self::assertTrue(true);
    }

    public function testItDoesNotStartProcessIfProcessAlreadyExists(): void
    {
        $commandExecutor = \Mockery::mock(CommandExecutorInterface::class)
            ->shouldReceive('execute')
            ->andReturn(new ExecutionResult(0, ['339397.test	(Detached)']))->getMock();
        $processManager = new ScreenProcessManager($commandExecutor);

        $this->expectException(ProcessAlreadyExistsException::class);
        $processManager->startProcess('test', 'ls');
    }

    public function testItFailsToStartProcessIfProcessFails(): void
    {
        $commandExecutor = \Mockery::mock(CommandExecutorInterface::class)
            ->shouldReceive('execute')->once()->andReturn(new ExecutionResult(0, []))->getMock()
            ->shouldReceive('execute')->once()
            ->with('screen -dm -S "test" ls')->andReturn(new ExecutionResult(1, ['error']))->getMock();
        $processManager = new ScreenProcessManager($commandExecutor);

        $this->expectExceptionMessage('error');
        $this->expectException(CannotStartProcessException::class);
        $processManager->startProcess('test', 'ls');
    }

    public function testItStopsProcess(): void
    {
        $commandExecutor = \Mockery::mock(CommandExecutorInterface::class)
            ->shouldReceive('execute')->once()->andReturn(
                new ExecutionResult(0, ['339397.test	(Detached)'])
            )->getMock()
            ->shouldReceive('execute')->once()
            ->with('screen -X -S "test" quit')->andReturn(new ExecutionResult(0, []))->getMock();
        $processManager = new ScreenProcessManager($commandExecutor);
        $processManager->stopProcess('test');
        self::assertTrue(true);
    }

    public function testItFailsToStopNotExistingProcess(): void
    {
        $commandExecutor = \Mockery::mock(CommandExecutorInterface::class)
            ->shouldReceive('execute')->once()->andReturn(
                new ExecutionResult(0, [])
            )->getMock();
        $processManager = new ScreenProcessManager($commandExecutor);
        $this->expectException(ProcessDoesNotExistException::class);
        $processManager->stopProcess('test');
    }
}
